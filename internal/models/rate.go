package models

import "time"

type Rates struct {
	Ticker    string    `json:"symbol"`
	CreatedAt time.Time `json:"created_at"`
}
