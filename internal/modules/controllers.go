package modules

import (
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/component"
	acontroller "gitlab.com/antoxa2614/go-rpc-gateway/internal/modules/auth/controller"
	rcontroller "gitlab.com/antoxa2614/go-rpc-gateway/internal/modules/rates/controller"
	ucontroller "gitlab.com/antoxa2614/go-rpc-gateway/internal/modules/user/controller"
)

type Controllers struct {
	Auth  acontroller.Auther
	User  ucontroller.Userer
	Rates rcontroller.Rater
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)
	rateController := rcontroller.NewRates(services.Rates, components)

	return &Controllers{
		Auth:  authController,
		User:  userController,
		Rates: rateController,
	}
}
