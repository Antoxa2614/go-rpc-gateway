package auth

import (
	"context"
	"log"
	"net/rpc"

	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/errors"
)

type AuthServiceJSONRPC struct {
	client *rpc.Client
}

func NewAuthServiceJSONRPC(client *rpc.Client) *AuthServiceJSONRPC {
	u := &AuthServiceJSONRPC{client: client}

	return u
}

func (t *AuthServiceJSONRPC) Register(ctx context.Context, in RegisterIn) RegisterOut {
	log.Printf("start auth register service")
	var out RegisterOut
	err := t.client.Call("AuthServiceJSONRPC.RegisterUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}
	log.Printf("auth register service; out:%+v", out)

	return out
}

func (t *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizeEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceVerifyErr
	}

	return out
}

func (t *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizeRefresh", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceRefreshTokenGenerationErr
	}

	return out
}

func (t *AuthServiceJSONRPC) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizePhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceVerifyErr
	}

	return out
}

func (t *AuthServiceJSONRPC) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	var out SendPhoneCodeOut
	err := t.client.Call("AuthServiceJSONRPC.SendPhoneCode", in, &out)
	if err != nil {
		out.Code = errors.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	var out VerifyEmailOut
	err := t.client.Call("AuthServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceVerifyErr
	}

	return out
}
