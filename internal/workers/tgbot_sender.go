package workers

import (
	"context"
	"time"

	"gitlab.com/antoxa2614/go-rpc-gateway/internal/provider"

	"go.uber.org/zap"
)

type TelegramBotWorker struct {
	messageBroker  provider.Consumerer
	telegramSender *provider.TGClient
}

func NewTelegramBotWorker(messageBroker provider.Consumerer, telegramSender *provider.TGClient) *TelegramBotWorker {
	return &TelegramBotWorker{messageBroker: messageBroker, telegramSender: telegramSender}
}

func (e *TelegramBotWorker) Run(ctx context.Context, log *zap.Logger) {
	msgs, err := e.messageBroker.Get()
	if err != nil {
		log.Fatal("Getting MessageBroker data error", zap.Error(err))
		return
	}
	go func() {
		for msg := range msgs {
			err = e.telegramSender.SendMessage(string(msg.Body))
			if err != nil {
				log.Fatal("Send telegram message error", zap.Error(err))
			}
			select {
			case <-ctx.Done():
				return
			default:
			}
		}
		time.Sleep(time.Millisecond * 100)
	}()
}
