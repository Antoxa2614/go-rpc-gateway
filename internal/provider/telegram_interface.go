package provider

import (
	"log"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type TGClient struct {
	bot    *tgbotapi.BotAPI
	chatID int64
}

// func NewTGClient(apiKey string, chatID int64) *TGClient {
func NewTGClient(apiKey string, chatID string) *TGClient {
	bot, err := tgbotapi.NewBotAPI(apiKey)
	if err != nil {
		log.Panic(err)
	}

	id, err := strconv.ParseInt(chatID, 10, 64)
	if err != nil {
		return nil
	}

	return &TGClient{
		bot:    bot,
		chatID: id,
	}
}

func (c *TGClient) SendMessage(text string) error {
	msg := tgbotapi.NewMessage(c.chatID, text)
	//msg.ParseMode = "Markdown"
	_, err := c.bot.Send(msg)
	return err
}
