package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"net/http"
	"net/rpc/jsonrpc"
	"os"

	"gitlab.com/antoxa2614/go-rpc-gateway/internal/workers"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/antoxa2614/go-rpc-gateway/config"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/component"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/errors"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/responder"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/router"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/server"
	internal "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service"
	aservice "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service/auth"
	rservice "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service/rates"
	uservice "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service/user"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/tools/cryptography"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/modules"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/provider"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf          config.AppConf
	logger        *zap.Logger
	srv           server.Server
	Sig           chan os.Signal
	Servises      *modules.Services
	MessageBroker provider.Consumerer
	TGClient      *provider.TGClient
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запуск worker'а
	tgBotWorker := workers.NewTelegramBotWorker(a.MessageBroker, a.TGClient)
	tgBotWorker.Run(ctx, a.logger)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация RabbitMQ Consumer
	a.MessageBroker = provider.NewConsumer(
		a.conf.RabbitMQ.Login,
		a.conf.RabbitMQ.Password,
		a.conf.RabbitMQ.Host,
		a.conf.RabbitMQ.Port,
		a.logger,
	)
	// инициализация Телеграм клиента
	a.TGClient = provider.NewTGClient(a.conf.TGClient.API, a.conf.TGClient.ChatID)
	if a.TGClient == nil {
		a.logger.Fatal("Telegram Client Initialization error", zap.Error(
			fmt.Errorf("conversion ChatID:%s to int64 error", a.conf.TGClient.ChatID)))
	}
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)

	// инициализация клиента для взаимодействия с сервисом пользователей
	var userRPCClient uservice.Userer
	if a.conf.UserGRPC.Host != "" {
		// инициализация grpc клиента
		client, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.UserGRPC.Host, a.conf.UserGRPC.Port),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init grpc client", zap.Error(err))
		}
		a.logger.Info("grpc client connected", zap.String("port", a.conf.UserGRPC.Port))
		userRPCClient = uservice.NewUserGRPCService(client)
	} else {
		// инициализация json rpc клиента
		client, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port))
		if err != nil {
			a.logger.Fatal("error init rpc client", zap.Error(err))
		}
		a.logger.Info("rpc client connected", zap.String("port", a.conf.UserRPC.Port))
		userRPCClient = uservice.NewUserServiceJSONRPC(client)
	}

	// инициализация клиента для взаимодействия с сервисом аутентификации
	var authRPCClient aservice.Auther
	if a.conf.AuthGRPC.Host != "" {
		// инициализация grpc клиента
		client, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.AuthGRPC.Host, a.conf.AuthGRPC.Port),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init grpc client", zap.Error(err))
		}
		a.logger.Info("grpc client connected", zap.String("port", a.conf.AuthGRPC.Port))
		authRPCClient = aservice.NewAuthGRPCService(client)
	} else {
		clientAuth, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.AuthRPC.Host, a.conf.AuthRPC.Port))
		if err != nil {
			a.logger.Fatal("error init rpc client", zap.Error(err))
		}
		a.logger.Info("auth rpc client connected", zap.String("port", a.conf.AuthGRPC.Port))
		authRPCClient = aservice.NewAuthServiceJSONRPC(clientAuth)
	}

	// инициализация grpc клиента для взаимодействия с сервисом курсов валют
	client, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.RatesGRPC.Host, a.conf.RatesGRPC.Port),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		a.logger.Fatal("error init grpc client", zap.Error(err))
	}
	a.logger.Info("grpc client connected", zap.String("port", a.conf.RatesGRPC.Port))
	ratesRPCClient := rservice.NewRatesClient(client)

	// инициализация сервисов
	services := modules.NewServices(userRPCClient, authRPCClient, ratesRPCClient)
	a.Servises = services

	controllers := modules.NewControllers(services, components)
	// инициализация роутера
	var r *chi.Mux = router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
