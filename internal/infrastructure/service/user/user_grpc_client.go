package user

import (
	"context"

	"gitlab.com/antoxa2614/go-rpc-gateway/internal/models"

	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/errors"
	ugrpc "gitlab.com/antoxa2614/go-rpc-gateway/rpc/user/grpc"
	"google.golang.org/grpc"
)

type UserGRPCService struct {
	client ugrpc.UsersClient
}

func NewUserGRPCService(client grpc.ClientConnInterface) *UserGRPCService {
	c := ugrpc.NewUsersClient(client)
	return &UserGRPCService{client: c}
}

func (t *UserGRPCService) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	var out UserCreateOut
	inService := ugrpc.UserCreateIn{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int32(in.Role),
	}
	outService, err := t.client.CreateUser(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.UserServiceCreateUserErr
		return out
	}

	out = UserCreateOut{
		UserID:    int(outService.UserID),
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}

func (t *UserGRPCService) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	var out UserUpdateOut
	inService := ugrpc.UserUpdateIn{
		User: &ugrpc.User{
			ID:            int32(in.User.ID),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int32(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
		Fields: in.Fields,
	}

	outService, err := t.client.Update(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
		return out
	}

	out = UserUpdateOut{
		Success:   outService.Success,
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}

func (t *UserGRPCService) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	var out UserUpdateOut

	inService := ugrpc.UserVerifyEmailIn{
		UserID: int32(in.UserID),
	}
	outService, err := t.client.VerifyEmail(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
		return out
	}

	out = UserUpdateOut{
		Success:   outService.Success,
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}

func (t *UserGRPCService) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	var out ChangePasswordOut

	inService := ugrpc.ChangePasswordIn{
		UserID:      int32(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	}
	outService, err := t.client.ChangePassword(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.UserServiceChangePasswordErr
		return out
	}

	out = ChangePasswordOut{
		Success:   outService.Success,
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}

func (t *UserGRPCService) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	var out UserOut

	inService := ugrpc.GetByEmailIn{
		Email: in.Email,
	}
	outService, err := t.client.GetByEmail(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
		return out
	}

	out = UserOut{
		User: &models.User{
			ID:            int(outService.User.ID),
			Name:          outService.User.Name,
			Phone:         outService.User.Phone,
			Email:         outService.User.Email,
			Password:      outService.User.Password,
			Role:          int(outService.User.Role),
			Verified:      outService.User.Verified,
			EmailVerified: outService.User.EmailVerified,
			PhoneVerified: outService.User.PhoneVerified,
		},
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}

func (t *UserGRPCService) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	var out UserOut

	inService := ugrpc.GetByPhoneIn{
		Phone: in.Phone,
	}
	outService, err := t.client.GetByPhone(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
		return out
	}

	out = UserOut{
		User: &models.User{
			ID:            int(outService.User.ID),
			Name:          outService.User.Name,
			Phone:         outService.User.Phone,
			Email:         outService.User.Email,
			Password:      outService.User.Password,
			Role:          int(outService.User.Role),
			Verified:      outService.User.Verified,
			EmailVerified: outService.User.EmailVerified,
			PhoneVerified: outService.User.PhoneVerified,
		},
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}

func (t *UserGRPCService) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	var out UserOut

	inService := ugrpc.GetByIDIn{
		UserID: int32(in.UserID),
	}
	outService, err := t.client.GetByID(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
		return out
	}

	out = UserOut{
		User: &models.User{
			ID:            int(outService.User.ID),
			Name:          outService.User.Name,
			Phone:         outService.User.Phone,
			Email:         outService.User.Email,
			Password:      outService.User.Password,
			Role:          int(outService.User.Role),
			Verified:      outService.User.Verified,
			EmailVerified: outService.User.EmailVerified,
			PhoneVerified: outService.User.PhoneVerified,
		},
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}

func (t *UserGRPCService) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	var out UsersOut

	ids := make([]int32, len(in.UserIDs))
	for i := 0; i < len(in.UserIDs); i++ {
		ids[i] = int32(in.UserIDs[i])
	}

	inService := ugrpc.GetByIDsIn{
		UserIDs: ids,
	}
	outService, err := t.client.GetByIDs(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
		return out
	}

	users := make([]models.User, len(outService.User))
	for i := 0; i < len(outService.User); i++ {
		users[i] = models.User{
			ID:            int(outService.User[i].ID),
			Name:          outService.User[i].Name,
			Phone:         outService.User[i].Phone,
			Email:         outService.User[i].Email,
			Password:      outService.User[i].Password,
			Role:          int(outService.User[i].Role),
			Verified:      outService.User[i].Verified,
			EmailVerified: outService.User[i].EmailVerified,
			PhoneVerified: outService.User[i].PhoneVerified,
		}
	}
	out = UsersOut{
		User:      users,
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}
