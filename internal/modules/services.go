package modules

import (
	aservice "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service/auth"
	rservice "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service/rates"
	uservice "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service/user"
)

type Services struct {
	User  uservice.Userer
	Auth  aservice.Auther
	Rates rservice.RatesClient
}

func NewServices(userService uservice.Userer, authService aservice.Auther, ratesService rservice.RatesClient) *Services {
	return &Services{
		User:  userService,
		Auth:  authService,
		Rates: ratesService,
	}
}
