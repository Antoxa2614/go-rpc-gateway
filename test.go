package main

import (
	"fmt"
	"os"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
)

type TGClient struct {
	bot    *tgbotapi.BotAPI
	chatID int64
}

func NewTGClient(apiKey string, chatID string) *TGClient {
	bot, err := tgbotapi.NewBotAPI(apiKey)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	id, err := strconv.ParseInt(chatID, 10, 64)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return &TGClient{
		bot:    bot,
		chatID: id,
	}
}

func (c *TGClient) SendMessage(text string) error {
	msg := tgbotapi.NewMessage(c.chatID, text)
	msg.ParseMode = "Markdown"
	_, err := c.bot.Send(msg)
	return err
}

func main() {
	err := godotenv.Load()
	fmt.Println(err)
	API := os.Getenv("TGCLIENT_API")
	ChatID := os.Getenv("TGCLIENT_CHATID")
	fmt.Printf("API:%s ChatID:%s\n", API, ChatID)
	c := NewTGClient(API, ChatID)
	err = c.SendMessage("I'm ready!")
	if err != nil {
		fmt.Println(err)
	}
	_ = c
}
