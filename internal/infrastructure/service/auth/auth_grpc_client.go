package auth

import (
	"context"

	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/errors"
	agrpc "gitlab.com/antoxa2614/go-rpc-gateway/rpc/auth/grpc"
	"google.golang.org/grpc"
)

type AuthGRPCService struct {
	client agrpc.AuthClient
}

func NewAuthGRPCService(client grpc.ClientConnInterface) *AuthGRPCService {
	c := agrpc.NewAuthClient(client)
	return &AuthGRPCService{client: c}
}

func (a *AuthGRPCService) Register(ctx context.Context, in RegisterIn) RegisterOut {
	var out RegisterOut
	inService := agrpc.RegisterIn{
		Email:          in.Email,
		Phone:          in.Phone,
		Password:       in.Password,
		IdempotencyKey: in.IdempotencyKey,
	}

	outService, err := a.client.Register(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
		return out
	}

	out = RegisterOut{
		Status:    int(outService.Status),
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}

func (a *AuthGRPCService) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	var out AuthorizeOut
	inService := agrpc.AuthorizeEmailIn{
		Email:             in.Email,
		Password:          in.Password,
		RemessagePassword: in.RetypePassword,
	}

	outService, err := a.client.AuthorizeEmail(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.AuthServiceVerifyErr
		return out
	}

	out = AuthorizeOut{
		UserID:       int(outService.UserID),
		AccessToken:  outService.AccessToken,
		RefreshToken: outService.RefreshToken,
		ErrorCode:    int(outService.ErrorCode),
	}
	return out
}

func (a *AuthGRPCService) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	var out AuthorizeOut
	inService := agrpc.AuthorizeRefreshIn{
		UserID: int32(in.UserID),
	}

	outService, err := a.client.AuthorizeRefresh(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.AuthServiceRefreshTokenGenerationErr
		return out
	}

	out = AuthorizeOut{
		UserID:       int(outService.UserID),
		AccessToken:  outService.AccessToken,
		RefreshToken: outService.RefreshToken,
		ErrorCode:    int(outService.ErrorCode),
	}
	return out
}

func (a *AuthGRPCService) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	var out AuthorizeOut
	inService := agrpc.AuthorizePhoneIn{
		Phone: in.Phone,
		Code:  int32(in.Code),
	}

	outService, err := a.client.AuthorizePhone(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.AuthServiceVerifyErr
		return out
	}

	out = AuthorizeOut{
		UserID:       int(outService.UserID),
		AccessToken:  outService.AccessToken,
		RefreshToken: outService.RefreshToken,
		ErrorCode:    int(outService.ErrorCode),
	}
	return out
}

func (a *AuthGRPCService) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	var out SendPhoneCodeOut
	inService := agrpc.SendPhoneCodeIn{
		Phone: in.Phone,
	}

	outService, err := a.client.SendPhoneCode(ctx, &inService)
	if err != nil {
		out.Code = errors.AuthServiceVerifyErr
		return out
	}

	out = SendPhoneCodeOut{
		Phone: outService.Phone,
		Code:  int(outService.Code),
	}
	return out
}

func (a *AuthGRPCService) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	var out VerifyEmailOut
	inService := agrpc.VerifyEmailIn{
		Hash:  in.Hash,
		Email: in.Email,
	}

	outService, err := a.client.VerifyEmail(ctx, &inService)
	if err != nil {
		out.ErrorCode = errors.AuthServiceVerifyErr
		return out
	}

	out = VerifyEmailOut{
		Success:   outService.Success,
		ErrorCode: int(outService.ErrorCode),
	}
	return out
}
