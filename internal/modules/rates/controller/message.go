package controller

import (
	service "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service/rates"
)

//go:generate easytags $GOFILE

type GetTickerResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type GetPricesResponse struct {
	Success   bool  `json:"success"`
	ErrorCode int   `json:"error_code,omitempty"`
	Data      PData `json:"data"`
}

type Data struct {
	Message string `json:"message"`
	Rates   map[string]*service.Symbol
}

type PData struct {
	Message string `json:"message"`
	Prices  map[string]float64
}
