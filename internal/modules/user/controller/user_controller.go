package controller

import (
	"net/http"

	"github.com/ptflp/godecoder"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/component"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/errors"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/handlers"
	"gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/responder"
	service "gitlab.com/antoxa2614/go-rpc-gateway/internal/infrastructure/service/user"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}
