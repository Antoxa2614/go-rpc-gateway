package provider

import (
	"fmt"

	amqp "github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

type Consumerer interface {
	//Get(ctx context.Context, message string) error
	Get() (<-chan amqp.Delivery, error)
}

type Consumer struct {
	ch    *amqp.Channel
	queue *amqp.Queue
}

func NewConsumer(login, password, host, port string, logger *zap.Logger) *Consumer {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", login, password, host, port))
	if err != nil {
		logger.Fatal("RabbitMQ connection error", zap.Error(err))
	}
	//defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		logger.Fatal("RabbitMQ create channel error", zap.Error(err))
	}
	//defer ch.Close()

	q, err := ch.QueueDeclare(
		"message", // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		logger.Fatal("RabbitMQ make queue error", zap.Error(err))
	}
	return &Consumer{ch: ch, queue: &q}
}

func (p *Consumer) Get() (<-chan amqp.Delivery, error) {
	msg, err := p.ch.Consume(
		p.queue.Name, // queue
		"",           // consumer
		true,         // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)
	return msg, err
}
